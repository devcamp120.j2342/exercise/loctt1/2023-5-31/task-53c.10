package models;

public abstract class Shape {
     private String color;

    public abstract double getArea();

    @Override
    public String toString() {
        return "Shape";
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
