import models.Retangle;
import models.Triangle;
public class ShapeDiagramClass{
    public static void main(String[] args) throws Exception {
        Retangle rectangle1 = new Retangle(5, 10);
        Retangle rectangle2 = new Retangle(10, 15);

        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());

        rectangle1.setColor("red");
        rectangle2.setColor("blue");

        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        Triangle triangle1 = new Triangle(5, 10);
        Triangle triangle2 = new Triangle(15, 10);

        System.out.println(triangle1.getArea());
        System.out.println(triangle2.getArea());

        triangle1.setColor("red");
        triangle2.setColor("blue");

        System.out.println(triangle1.toString());
        System.out.println(triangle2.toString());
    }
}
